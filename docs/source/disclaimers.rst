Disclaimers
===========
This chapter contains some general information that users of krapplet 
should be aware of.


Notification icon
~~~~~~~~~~~~~~~~~
Notification icons are an officially deprecated method under GTK3.
Use of the notification icon may or may not be abandoned in future releases.


Systray
~~~~~~~
Krapplet currently will not work properly on desktops that do not provide a
systray.


Password generation
~~~~~~~~~~~~~~~~~~~
The generation of the password makes use of Python's random functions.
These random functions rely on operating system features, typically the
current time in fractions of seconds, or when available from a random
device like /dev/urandom in Linux, which seems better.
Even better would be if there is a hardware random number generator
available and used by the operating system. How Python's random function
works therefore depends on the Python interpretor, the system configuration,
and the hardware. 

The generated secret may not be good enough for real cryptographic purposes.
Whether it is good enough for password generation is left to users of
krapplet to decide.


Clipboard usage
~~~~~~~~~~~~~~~
Krapplet can copy secrets to the system clipboard, allowing a user to
paste a secret in an input field, seemingly a secure way since
bystanders cannot see what is being copied.
However, please be weary of clipboard managers maintaining a clipboard
history and therefore can reveal everything that has been copied to it,
including passwords.
Note that krapplet clears the clipboard after a key window is closed to
prevent a password lingering in the clipboard, and getting accidentally
pasted in a place where others might see it.


Swap
~~~~
Memory constrained systems might swap krapplet out to a storage device.
Encrypt your swap device or swap file to prevent hackers from harvesting
secrets from it.


