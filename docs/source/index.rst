Krapplet
========

Krapplet is a graphical password manager for Linux.
Its name derives from 'keyring applet'. 
Originally krapplet was purely built as a frontend for gnome-keyring, but now
it also supports another mechanism for storing passwords, a format compatible
with the `pass <https://www.passwordstore.org/>`_ command line password
manager.


Table of contents
~~~~~~~~~~~~~~~~~

.. toctree::
   introduction
   screenshots
   installation
   usage
   storage-providers
   disclaimers
   trouble-shooting
   license
   revision-history

