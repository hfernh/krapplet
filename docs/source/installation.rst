Installation
============
Krapplet is developed in Python, so there is no compilation needed.


Installation sources
~~~~~~~~~~~~~~~~~~~~
Krapplet is available:

- As source code on `Gitlab <https://gitlab.com/hfernh/krapplet>`__ 
- From the Python Package Index `PyPI <https://pypi.org/project/krapplet>`__
- From the Arch Linux' User Repository `AUR
  <https://aur.archlinux.org/packages/krapplet>`__ 
- From the Gentoo Linux' User Repository `GURU
  <https://gitweb.gentoo.org/repo/proj/guru.git/tree/app-admin/krapplet>`__.


Software dependencies
~~~~~~~~~~~~~~~~~~~~~
Krapplet makes use of the following software:

- `Python <https://www.python.org/>`__ as the programming language, tested
  with versions 3.7, 3.8 and 3.9. Krapplet does not support Python 2.7.
- `GTK3 <https://www.gtk.org/>`__ for the user interface. Developed against
  version 3.24.22. Please refer to `these instructions
  <https://pygobject.readthedocs.io/en/latest/getting_started.html>`__
  for how to install GTK3 for your Linux distribution.
- `Secretstorage <https://github.com/mitya57/secretstorage>`__ as the
  Python API towards when using gnome-keyring as the storage provider.
  Secretstorage can be installed via `PyPI
  <https://pypi.org/project/SecretStorage>`__, but is also available
  as an operating system package in many Linux distributions.
  Secretstorage makes use of a Python package `cryptography 
  <https://github.com/pyca/cryptography>`__, which in turn
  requires some operating system packages to be installed: see
  `here <https://cryptography.io/en/latest/installation.html>`__ for
  installation instructions.
- `Python-gnupg <https://docs.red-dove.com/python-gnupg>`__ when using the
  pass storage provider. Python-gnupg is a python wrapper around
  `GunPG <https://gnupg.org>`__, which is used for the actual encryption
  of keys, and should be installed as well


Notes on installing using PyPI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PyPI is a very convenient way of installing Python packages and programs. 
However, be aware that:

- Dependencies may not always fully resolve, e.g. GTK3 cannot be installed 
  from PypI.
- Indirect dependencies that are not written in Python / are not available on
  PyPI won't be installed, e.g. GnuPG, or Gnome-Keyring.
- Anyone can put anything on PyPI: there is no control whether the right
  software is installed.
  There is for example also a package called gnupg on PyPI, which is a fork of
  python-gnupg, but which does not work for krapplet.
 

Krapplet has optional dependencies of which at least one need to be selected.
When using krapplet with gnome-keyring then use::

  # pip install krapplet[gnomekeyring]

When using krapplet with pass::

  # pip install krapplet[pass]

Or when both storage providers are wanted::

  # pip install krapplet[gnomekeyring,pass]


The recommendation is to install krapplet as an operating package; the person
who packaged krapplet will have made sure all dependencies will be installed.
Unfortunately not all operating systems provide a krapplet package, in which
case installing from PyPI may be the next best option.
