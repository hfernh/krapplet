Introduction
============


Why use a password manager
~~~~~~~~~~~~~~~~~~~~~~~~~~
In today's on-line world, people need to maintain lots of passwords,
particularly to access websites.
These passwords need to have a certain complexity, so that it is hard to
guess them, or crack them.
However, complex passwords are hard to remember, and therefore people write
them down on a piece of paper or in text files.·
Such written down passwords are not very secure though.
Password managers were invented to store complex passwords in a safe and
secure manner.


Krapplet goals and features
~~~~~~~~~~~~~~~~~~~~~~~~~~~
The aim of krapplet is to be a Linux native password manager.
Krapplet provides the following features:

- simple to use
- sitting in the systray: ready to use whenever you need it
- tries not to be in the user's way; uses only a small amount of
  user's computer screen
- flexible: store also associated information, like a username, e-mail
  address, and the website URL
- uses common Linux manners to store and secure passwords, either gnome-keyring or GPG
- built in password generator


Concept
~~~~~~~
Password aka secrets need to be stored in a safe manner, i.e. encrypted.·
A well established mechanism for that in the Linux world is·
`gnome-keyring <https://github.com/GNOME/gnome-keyring>`_.
Gnome-keyring organizes secrets in keyrings, often two keyrings exist:

- Login: which can be opened once a user logs in, and in which a user can·
  store the secrets to open other keyrings.
- Default: typically used for application secrets

Krapplet builds on this concept, allowing not only passwords to be stored,·
but also other information like username, url, email address, et

Alternatively, krapplet also support the storage format adopted by 
`pass <https://www.passwordstore.org/>`_, which encrypts key files in a 
directory structure under ``${HOME}/.password-store`` using
`GunPG <https://gnupg.org/>`_.


Environment
~~~~~~~~~~~
Krapplet is currently a Linux only application.
Krapplet has been tested on the amd64 and x64 architecture, but might work
on other architectures as well.
A system tray needs to be available for the applet to embed itself in.
