Revision history
================

Version 0.4.0 - 10 June 2023
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New functionalities:
- Confirm key delete
- Add an option for backup and restore

Version 0.3.0/1 - 21 February 2021
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New functionalities:
- Secret validity added
- Secret complexity indicator added
- Functionality added to move secrets from one keyring to another
- Auto-unlock keyrings when they are in the login keyring
- Prevent deleting keyrings with keys attached to them
- Generate a secret when a new key is created
- Add `pass https://www.passwordstore.org/`_ as a storage provider

Version 0.2.0 - 17 January 2021
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Updates include:

- Graceful exit on Ctrl-C: no longer Python crash log
- Clear clipboard after window close

Version 0.1.0 - 29 December 2020
~~~~~~~~~~~~~~~~~~~~~~~~~~

Initial official release of krapplet,
a password manager based on gnome-keyring.

Features of krapplet include:

- Systray applet: with a sinple right click you have the neccessary login
  data immediately available
- keys are organized in keyrings, containing not only secrets (passwords),
  but also other related information (attributes)


