Screenshots
===========

Krapplet in the systray
~~~~~~~~~~~~~~~~~~~~~~~
Once krapplet is loaded it will show itself as a keyring with some keys:

.. image:: krapplet-in-systray.png
   :align: center



Main menu
~~~~~~~~~
The main menu is what you see once the systray icon is right-clicked:

.. image:: krapplet-main-menu.png
   :align: center



Keyrings
~~~~~~~~
Hovering over a keyring will show the keys in a keyring:

:. image:: krapplet-submenu.png
   :align: center



Key window
~~~~~~~~~~
The key window gives all information about a key:

- The keyring it belongs to.
  This combobox can be used to move a key to another keyring.
- The name of the key
- Some attributes: other information that might be relevant.
  There is a button to add more attributes. Clearing the attribute name
  removes the attribute from the key.
  If there is an attribute called URL (in capitals), then there will also
  be a launch button.
- The secret, aka password.
  Under the secret there is a little red/amber/green bar indicating the
  complexity of the password.
  There is a button to copy the secret to the clipboard, one to show the 
  secret, and one to generate a new secret..

.. image:: krapplet-keywindow.png
   :align: center



Generate password
~~~~~~~~~~~~~~~~~
Generation of passwords is based on the total requested lenght, and the
minimum number of uppercase, lowercase, numeric and special characters:

.. image:: krapplet-gen-pw.png
   :align: center
