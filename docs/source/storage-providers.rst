Storage providers
=================

Krapplet uses by default gnome-keyring as storage provider.
As of version 0.3.0 krapplet also supports the storage architecture of
`pass <https://www.passwordstore.org>`_.

Gnome-keyring
~~~~~~~~~~~~~
Using gnome-keyring is default, but can be explicitely selected by passing
this commandline parameter: ``--storage gnome-keyring``.

How gnome-keyring stores secrets is described in `this gnome-keyring
wiki page <https://wiki.gnome.org/Projects/GnomeKeyring/StoringSecrets>`__.

The upshot is that:

- There should be a password set for the first keyring to be unlocked,
  the login keyring
- That password should be the same as the password for the user's Linux
  account

Desktop environments are capable of unlocking the login keyring as long
as auto-login is not used.


Pass
~~~~
Start krapplet with ``--storage pass``: to select the pass compatible
storage provider.

Note that the pass software code itself is not used; pass is written in bash,
whereas krapplet is written in Python.
Krapplet just mimics the way how pass stores passwords.

For the pass compatible storage provider there are the following
optional command line switches:

- ``--pgp-id <pgp-id>``: which pgp-id to use
- ``--armor``: encode the password files in ASCII.

Pass and the krapplet pass compatible storage provider store their passwords
in ``${HOME}/.password-store``.
Krapplet assumes this directory has been initialized before using the command
``pass init``. 
This creates a file ``.gpg-id`` in this directory
``${HOME}/.password-store``.
In order to use the pass compatible storage provider gpg keys should be set
up.
Pass uses
`PGP <https://en.wikipedia.org/wiki/Pretty_Good_Privacy>`_ /
`RFC4880 <https://tools.ietf.org/html/rfc4880>`_
encoded password files.
