Troubleshooting
---------------

Error: could not embed in systray
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Make sure that there is a systray for krapplet to embed itself in. This 
   behavior has been seen on GNOME-3 desktops.

Cannot see the krapplet icon
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Verify that there are krapplet.png files in
   /usr/share/icons/hicolor/48x48/apps and in
   /usr/share/icons/hicolor/96x96/apps
-  Refresh the icon cache by running gtk-update-icon-cache

Login keyring not automatically unlocked
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Please check the official `gnome-keyring
   wiki <https://wiki.gnome.org/Projects/GnomeKeyring/Pam>`__, or your
   distribution's documentation, e.g. for `Arch
   Linux <https://wiki.archlinux.org/index.php/GNOME/Keyring>`__,

