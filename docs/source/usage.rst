Usage
=====

Once krapplet is started it will show itself as an icon in the system
tray.
When this icon is right clicked it will show a menu with the
available keyrings, the option to add a keyring, and  the backup, restore,
about and quit options.

When hovering over a keyring it will give the option to unlock
a keyring when it is locked or show the availabe keys, and options to
add a key, remove the keyring, or lock the keyring.

Once a key has been selected it will show a small floating window to
manipulate the key.
It shows the key name as a first input field and
when it is not a new key also the create and modified timestamps.

Next are the key attributes in name/value pairs.
There is a button to add an additional attribute.
A blank attribute name is interpreted as an instruction to remove that
attribute.
When an attribute name "URL" is present a launch button will be shown,
which opens the system webbrowser for the listed URL when clicked.

The "secret" section contains the password, by default shown hidden.
This section has buttons to generate a new password, copy the password
to the system clipboard, and to show or hide the password.

The generation of a new password is based on five parameters: 

- total lenght
- minimum number of uppercase, lowercase, numeric and special
  characters; use zero to exclude a category.

Special characters are defined as: !#$%&()\*+,-./:;<=>?@[\\]^\_\`{\|}~

A colored bar may be seen under the secret: it is a password complexity
indicator, which colors

- red when the password is too easy to crack, e.g. consisting of a mix of 
  nine or less uppercase, lowercase, numeric, and special characters 
- amber when there are ten or 11 characters of that same mix 
- green when 12 or more.

The complexity indicator is based on its entropy.

From the main menu it is possible to backup or restore all keyrings in
a password protected encrypted keyfile from/at a desired location.
