#!/usr/bin/python3

"""
krapplet: A password manager written as a gnome-keyring applet
krapplet.py is the main program
(c) 2020-2023 Johannes Willem Fernhout, BSD 3-Clause License applies
"""
