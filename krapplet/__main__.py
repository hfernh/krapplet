#!/usr/bin/python3
"""
krapplet: A password manager written as a gnome-keyring applet
(c) 2020-2023 Johannes Willem Fernhout, BSD 3-Clause License applies
"""

from . import krapplet

krapplet.main()
