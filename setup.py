#!/usr/bin/env python
"""
krapplet: A password manager written as a gnome-keyring applet
setup.py is the PyPI packaging script
(c) 2020-2021 Johannes Willem Fernhout, BSD 3-Clause License applies
"""
from setuptools import setup

if __name__ == "__main__":
    setup()

